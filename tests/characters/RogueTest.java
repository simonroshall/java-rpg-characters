package characters;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RogueTest {

    @Test
    public void getLevel_rogueLevel_shouldReturn1() {
        //arrange
        Rogue rogue = new Rogue("test");
        int expected = 1;

        //act
        int actual = rogue.getLevel();

        //assert
        assertEquals(expected, actual);
    }
    @Test
    public void levelUp_rogueLevelUp_shouldReturn2() {
        //arrange
        Rogue rogue = new Rogue("test");
        int expected = 2;

        //act
        rogue.levelUp();
        int actual = rogue.getLevel();

        //assert
        assertEquals(expected, actual);
    }
    @Test
    public void getPrimaryAttributes_rogueStrength_shouldReturn2() {
        //arrange
        Rogue rogue = new Rogue("test");
        int expected = 2;

        //act
        int actual = rogue.getPrimaryAttributes().strength;

        //assert
        assertEquals(expected, actual);
    }
    @Test
    public void getPrimaryAttributes_rogueDexterity_shouldReturn6() {
        //arrange
        Rogue rogue = new Rogue("test");
        int expected = 6;

        //act
        int actual = rogue.getPrimaryAttributes().dexterity;

        //assert
        assertEquals(expected, actual);
    }
    @Test
    public void getPrimaryAttributes_rogueIntelligence_shouldReturn1() {
        //arrange
        Rogue rogue = new Rogue("test");
        int expected = 1;

        //act
        int actual = rogue.getPrimaryAttributes().intelligence;

        //assert
        assertEquals(expected, actual);
    }
    @Test
    public void getPrimaryAttributesOnLevelUp_rogueLevelUp_shouldReturn3_10_2() {
        //arrange
        Rogue rogue = new Rogue("test");
        int expectedStrength = 3;
        int expectedDexterity = 10;
        int expectedIntelligence = 2;

        //act
        rogue.levelUp();
        int actualStrength = rogue.getPrimaryAttributes().strength;
        int actualDexterity = rogue.getPrimaryAttributes().dexterity;
        int actualIntelligence = rogue.getPrimaryAttributes().intelligence;

        //assert
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
    }
}