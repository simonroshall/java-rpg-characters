package characters;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RangerTest {

    @Test
    public void getLevel_rangerLevel_shouldReturn1() {
        //arrange
        Ranger ranger = new Ranger("test");
        int expected = 1;

        //act
        int actual = ranger.getLevel();

        //assert
        assertEquals(expected, actual);
    }
    @Test
    public void levelUp_rangerLevelUp_shouldReturn2() {
        //arrange
        Ranger ranger = new Ranger("test");
        int expected = 2;

        //act
        ranger.levelUp();
        int actual = ranger.getLevel();

        //assert
        assertEquals(expected, actual);
    }
    @Test
    public void getPrimaryAttributes_rangerStrength_shouldReturn1() {
        //arrange
        Ranger ranger = new Ranger("test");
        int expected = 1;

        //act
        int actual = ranger.getPrimaryAttributes().strength;

        //assert
        assertEquals(expected, actual);
    }
    @Test
    public void getPrimaryAttributes_rangerDexterity_shouldReturn7() {
        //arrange
        Ranger ranger = new Ranger("test");
        int expected = 7;

        //act
        int actual = ranger.getPrimaryAttributes().dexterity;

        //assert
        assertEquals(expected, actual);
    }
    @Test
    public void getPrimaryAttributes_rangerIntelligence_shouldReturn1() {
        //arrange
        Ranger ranger = new Ranger("test");
        int expected = 1;

        //act
        int actual = ranger.getPrimaryAttributes().intelligence;

        //assert
        assertEquals(expected, actual);
    }
    @Test
    public void getPrimaryAttributesOnLevelUp_rangerLevelUp_shouldReturn2_12_2() {
        //arrange
        Ranger ranger = new Ranger("test");
        int expectedStrength = 2;
        int expectedDexterity = 12;
        int expectedIntelligence = 2;

        //act
        ranger.levelUp();
        int actualStrength = ranger.getPrimaryAttributes().strength;
        int actualDexterity = ranger.getPrimaryAttributes().dexterity;
        int actualIntelligence = ranger.getPrimaryAttributes().intelligence;

        //assert
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
    }
}