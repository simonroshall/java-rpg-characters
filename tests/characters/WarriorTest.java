package characters;

import enums.ArmorType;
import enums.Slot;
import enums.WeaponType;
import exceptions.InvalidArmorException;
import exceptions.InvalidWeaponException;
import items.Armor;
import items.Weapon;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WarriorTest {

    @Test
    public void getLevel_warriorLevel_shouldReturn1() {
        //arrange
        Warrior warrior = new Warrior("test");
        int expected = 1;

        //act
        int actual = warrior.getLevel();

        //assert
        assertEquals(expected, actual);
    }
    @Test
    public void levelUp_warriorLevelUp_shouldReturn2() {
        //arrange
        Warrior warrior = new Warrior("test");
        int expected = 2;

        //act
        warrior.levelUp();
        int actual = warrior.getLevel();

        //assert
        assertEquals(expected, actual);
    }
    @Test
    public void getPrimaryAttributes_warriorStrength_shouldReturn5() {
        //arrange
        Warrior warrior = new Warrior("test");
        int expected = 5;

        //act
        int actual = warrior.getPrimaryAttributes().strength;

        //assert
        assertEquals(expected, actual);
    }
    @Test
    public void getPrimaryAttributes_warriorDexterity_shouldReturn2() {
        //arrange
        Warrior warrior = new Warrior("test");
        int expected = 2;

        //act
        int actual = warrior.getPrimaryAttributes().dexterity;

        //assert
        assertEquals(expected, actual);
    }
    @Test
    public void getPrimaryAttributes_warriorIntelligence_shouldReturn1() {
        //arrange
        Warrior warrior = new Warrior("test");
        int expected = 1;

        //act
        int actual = warrior.getPrimaryAttributes().intelligence;

        //assert
        assertEquals(expected, actual);
    }
    @Test
    public void getPrimaryAttributesOnLevelUp_rangerLevelUp_shouldReturn8_4_2() {
        //arrange
        Warrior warrior = new Warrior("test");
        int expectedStrength = 8;
        int expectedDexterity = 4;
        int expectedIntelligence = 2;

        //act
        warrior.levelUp();
        int actualStrength = warrior.getPrimaryAttributes().strength;
        int actualDexterity = warrior.getPrimaryAttributes().dexterity;
        int actualIntelligence = warrior.getPrimaryAttributes().intelligence;

        //assert
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
    }
    @Test
    public void equipWeapon_weaponLevelTooHigh_shouldThrowInvalidWeaponException(){
        //arrange
        Warrior warrior = new Warrior("test");
        Weapon weapon = new Weapon("Axe",2, Slot.WEAPON, WeaponType.AXE,10,2);

        //act & assert
        assertThrows(InvalidWeaponException.class,() -> warrior.checkEquipWeapon(weapon));
    }
    @Test
    public void equipArmor_armorLevelTooHigh_shouldThrowInvalidArmorException(){
        //arrange
        Warrior warrior = new Warrior("test");
        Armor armor = new Armor("Common plate body armor",2, Slot.BODY,ArmorType.PLATE,new PrimaryAttributes(1,0,0));

        //act & assert
        assertThrows(InvalidArmorException.class,() -> warrior.checkEquipArmor(armor));
    }
    @Test
    public void equipWeapon_wrongWeaponType_shouldThrowInvalidWeaponException(){
        //arrange
        Warrior warrior = new Warrior("test");
        Weapon weapon = new Weapon("Common bow",1, Slot.WEAPON, WeaponType.BOW,10,2);

        //act & assert
        assertThrows(InvalidWeaponException.class,() -> warrior.checkEquipWeapon(weapon));
    }
    @Test
    public void equipArmor_wrongArmorType_shouldThrowInvalidArmorException(){
        //arrange
        Warrior warrior = new Warrior("test");
        Armor armor = new Armor("Common cloth head armor",1, Slot.HEAD,ArmorType.CLOTH,new PrimaryAttributes(0,0,5));

        //act & assert
        assertThrows(InvalidArmorException.class,() -> warrior.checkEquipArmor(armor));
    }
    @Test
    public void equipWeapon_warriorEquipsWeapon_shouldReturnTrue() {
        //arrange
        Warrior warrior = new Warrior("test");
        Weapon weapon = new Weapon("Axe",1, Slot.WEAPON, WeaponType.AXE,10,2);
        boolean expected = true;

        //act
        warrior.equipWeapon(weapon);

        //assert
        assertEquals(expected,warrior.getEquipment().containsValue(weapon));
    }
    @Test
    public void equipArmor_warriorEquipsArmor_shouldReturnTrue() {
        //arrange
        Warrior warrior = new Warrior("test");
        Armor armor = new Armor("Common plate body armor",1, Slot.BODY,ArmorType.PLATE,new PrimaryAttributes(1,0,0));
        boolean expected = true;

        //act
        warrior.equipArmor(armor);

        //assert
        assertEquals(expected,warrior.getEquipment().containsValue(armor));
    }
    @Test
    public void calculateDPSWithoutWeapon_warriorDPS_shouldReturn() {
        //arrange
        Warrior warrior = new Warrior("test");
        int expected = 1*(1+(5/100));

        //act
        double actual = warrior.getCharacterDPS();

        //assert
        assertEquals(expected, actual);
    }
    @Test
    public void calculateDPSWithWeapon_warriorDPS_shouldReturn() {
        //arrange
        Warrior warrior = new Warrior("test");
        Weapon weapon = new Weapon("Axe",1, Slot.WEAPON, WeaponType.AXE,7,1.1);
        double expected = (7*1.1)*(1+(5/100));

        //act
        warrior.equipWeapon(weapon);
        double actual = warrior.getCharacterDPS();

        //assert
        assertEquals(expected, actual);
    }
    @Test
    public void calculateDPSWithWeaponAndArmor_warriorDPS_shouldReturn() {
        //arrange
        Warrior warrior = new Warrior("test");
        Weapon weapon = new Weapon("Axe",1, Slot.WEAPON, WeaponType.AXE,7,1.1);
        Armor armor = new Armor("Common plate body armor",1, Slot.BODY,ArmorType.PLATE,new PrimaryAttributes(1,0,0));
        double expected = (7*1.1)*(1+((5+1)/100));

        //act
        warrior.equipWeapon(weapon);
        warrior.equipArmor(armor);
        double actual = warrior.getCharacterDPS();

        //assert
        assertEquals(expected, actual);
    }
}