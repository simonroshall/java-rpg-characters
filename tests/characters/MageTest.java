package characters;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MageTest {
    @Test
    public void getLevel_mageLevel_shouldReturn1() {
        //arrange
        Mage mage = new Mage("test");
        int expected = 1;

        //act
        int actual = mage.getLevel();

        //assert
        assertEquals(expected, actual);
    }

    @Test
    public void levelUp_mageLevelUp_shouldReturn2() {
        //arrange
        Mage mage = new Mage("test");
        int expected = 2;

        //act
        mage.levelUp();
        int actual = mage.getLevel();

        //assert
        assertEquals(expected, actual);
    }
    @Test
    public void getPrimaryAttributes_mageStrength_shouldReturn1() {
        //arrange
        Mage mage = new Mage("test");
        int expected = 1;

        //act
        int actual = mage.getPrimaryAttributes().strength;

        //assert
        assertEquals(expected, actual);
    }
    @Test
    public void getPrimaryAttributes_mageDexterity_shouldReturn1() {
        //arrange
        Mage mage = new Mage("test");
        int expected = 1;

        //act
        int actual = mage.getPrimaryAttributes().dexterity;

        //assert
        assertEquals(expected, actual);
    }
    @Test
    public void getPrimaryAttributes_mageIntelligence_shouldReturn8() {
        //arrange
        Mage mage = new Mage("test");
        int expected = 8;

        //act
        int actual = mage.getPrimaryAttributes().intelligence;

        //assert
        assertEquals(expected, actual);
    }
    @Test
    public void getPrimaryAttributesOnLevelUp_mageLevelUp_shouldReturn2_2_13() {
        //arrange
        Mage mage = new Mage("test");
        int expectedStrength = 2;
        int expectedDexterity = 2;
        int expectedIntelligence = 13;

        //act
        mage.levelUp();
        int actualStrength = mage.getPrimaryAttributes().strength;
        int actualDexterity = mage.getPrimaryAttributes().dexterity;
        int actualIntelligence = mage.getPrimaryAttributes().intelligence;

        //assert
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
    }
}