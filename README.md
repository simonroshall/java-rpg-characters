# 👏 RPG Characters

This i a projct made in Java. This project can be used as the backend to a rpg-game. 

## ✌️ Description

### 🔥 About the application

This is a small Java application witch can be further developed by adding a game interface. 
The application has for example the following logic: 
* Create characters of types Mage, Warrior, Ranger and Rogue. 
* Create weapon and armor and equip them to a character. 
* Level up a character. 
* Calculate a characters damage. 

### 🤖 Technologies

* Java jdk-17
* Intellij Idea
* JUnit 5

### ⚠️ Dependencies

* We recommend to not change the versions in the dependencies. Otherwise, the application might not work. 

### ⚡ Installing

To get the application running, simply clone the repo below: 
```
git clone https://gitlab.com/simonroshall/java-rpg-characters
```

### 💻 Executing program

How to run the application:
```
* Run the main class
```
## 😎 Authors

Contributors and contact information

Simon Roshäll 
[@simonroshall](https://gitlab.com/simonroshall/)

## 🦝 Version History

* 0.1
    * Initial Release

## 🌌 License

This project is fully free for use.
