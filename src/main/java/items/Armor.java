package items;

import characters.PrimaryAttributes;
import enums.ArmorType;
import enums.Slot;

public class Armor extends Item {
    private ArmorType armorType;
    private PrimaryAttributes primaryAttributes;

    public Armor() {
    }

    public Armor(String name, int requiredLevel, Slot slot, ArmorType armorType, PrimaryAttributes primaryAttributes) {
        super(name, requiredLevel, slot);
        this.armorType = armorType;
        this.primaryAttributes = primaryAttributes;
    }

    public ArmorType getArmorType() {
        return armorType;
    }

    public void setArmorType(ArmorType armorType) {
        this.armorType = armorType;
    }

    public PrimaryAttributes getPrimaryAttributes() {
        return primaryAttributes;
    }

    public void setAttribute(PrimaryAttributes primaryAttributes) {
        this.primaryAttributes = primaryAttributes;
    }
}
