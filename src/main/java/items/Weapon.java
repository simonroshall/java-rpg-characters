package items;

import enums.Slot;
import enums.WeaponType;

public class Weapon extends Item {
    private WeaponType weaponType;
    private double damage;
    private double attacksPerSecond;
    private double damagePerSecond;

    public Weapon() {
    }

    public Weapon(String name, int requiredLevel, Slot slot, WeaponType weaponType, double damage, double attacksPerSecond) {
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.slot = slot;
        this.weaponType = weaponType;
        this.damage = damage;
        this.attacksPerSecond = attacksPerSecond;
        this.damagePerSecond = damage * attacksPerSecond;
    }

    public WeaponType getWeaponType() {
        return weaponType;
    }

    public void setWeaponType(WeaponType weaponType) {
        this.weaponType = weaponType;
    }

    public double getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public double getAttacksPerSecond() {
        return attacksPerSecond;
    }

    public void setAttacksPerSecond(int attacksPerSecond) {
        this.attacksPerSecond = attacksPerSecond;
    }

    public double getDamagePerSecond() {
        return damagePerSecond;
    }

    public void setDamagePerSecond() {
        this.damagePerSecond = this.damage * this.attacksPerSecond;
    }
}
