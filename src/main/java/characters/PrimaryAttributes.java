package characters;

public class PrimaryAttributes {
    protected int strength;
    protected int dexterity;
    protected int intelligence;

    public PrimaryAttributes() {
    }

    public PrimaryAttributes(int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    public int getAttribute(String attribute) {

        if (attribute.equals("strength")) {
            return strength;
        }
        if (attribute.equals("dexterity")) {
            return dexterity;
        }
        if (attribute.equals("intelligence")) {
            return intelligence;
        }
        return 0;
    }
}