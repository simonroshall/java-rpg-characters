package characters;

import exceptions.InvalidArmorException;
import exceptions.InvalidWeaponException;
import items.Armor;
import items.Weapon;

public class Rogue extends Character {

    public Rogue() {
    }

    public Rogue(String name) {
        super(name, 1, 6, 0,"dexterity",2,6,1);
    }

    @Override
    public void levelUp() {
        this.level++;
        this.basePrimaryAttributes += 4;
        this.totalPrimaryAttributes += 4;
        this.primaryAttributes.strength += 1;
        this.primaryAttributes.dexterity += 4;
        this.primaryAttributes.intelligence += 1;
    }

    @Override
    public void checkEquipWeapon(Weapon weapon) throws InvalidWeaponException {
        if(weapon.getWeaponType().toString().equals("DAGGER") || weapon.getWeaponType().toString().equals("SWORD") && weapon.getSlot().toString().equals("WEAPON") && this.getLevel() >= weapon.getRequiredLevel()) {
            System.out.println(weapon.getName() + " is equipped!");
        }else{
            throw new InvalidWeaponException("\n" + "You cannot add this weapon!");
        }
    }
    @Override
    public void checkEquipArmor(Armor armor) throws InvalidArmorException {
        if(armor.getArmorType().toString().equals("LEATHER") || armor.getArmorType().toString().equals("MAIL") && this.getLevel() >= armor.getRequiredLevel()) {
            System.out.println(armor.getName() + " is equipped!");
        }else {
            throw new InvalidArmorException("\n" + "You cannot add this armor!");
        }
    }
}
