package characters;

import enums.Slot;
import items.Armor;
import items.Item;
import items.Weapon;
import java.util.EnumMap;

public abstract class Character implements ICharacter {
    protected String name;
    protected int level;
    protected int basePrimaryAttributes;
    protected int totalPrimaryAttributes;
    protected String primaryAttributeType;
    protected PrimaryAttributes primaryAttributes;
    protected EnumMap<Slot,Item> equipment;

    public Character() {
    }

    public Character(String name, int level, int basePrimaryAttributes, int totalPrimaryAttributes, String primaryAttributeType,int strength,int dexterity, int intelligence) {
        this.name = name;
        this.level = level;
        this.basePrimaryAttributes = basePrimaryAttributes;
        this.totalPrimaryAttributes = totalPrimaryAttributes;
        this.primaryAttributeType = primaryAttributeType;
        this.primaryAttributes = new PrimaryAttributes(strength,dexterity,intelligence);
        this.equipment = new EnumMap<>(Slot.class);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel() {
        this.level++;
    }

    public int getBasePrimaryAttributes() {
        return basePrimaryAttributes;
    }

    public  void setBasePrimaryAttributes() {
        this.basePrimaryAttributes++;
    }
    public void setTotalPrimaryAttributes(int totalPrimaryAttributes) {
        this.totalPrimaryAttributes += totalPrimaryAttributes;
    }

    public int getTotalPrimaryAttributes() {
        totalPrimaryAttributes = basePrimaryAttributes;
        setTotalPrimaryAttributes();
        return totalPrimaryAttributes;
    }

    public PrimaryAttributes getPrimaryAttributes() {
        return primaryAttributes;
    }

    public void setPrimaryAttributes(PrimaryAttributes primaryAttributes) {
        this.primaryAttributes.strength += primaryAttributes.strength;
        this.primaryAttributes.dexterity += primaryAttributes.dexterity;
        this.primaryAttributes.intelligence += primaryAttributes.intelligence;
    }
    public String getPrimaryAttributeType() {
        return primaryAttributeType;
    }

    public EnumMap<Slot, Item> getEquipment() {
        return equipment;
    }

    public void setEquipment(EnumMap<Slot, Item> equipment) {
        this.equipment = equipment;
    }
    public void printStatistics() {
        String stats =
                "......................." + "\n" + "Character name: " + getName() + "\n" + "Level: " + getLevel() + "\n" +
                "Strength: " + this.primaryAttributes.strength + "\n" + "Dexterity: " + this.primaryAttributes.dexterity + "\n" +
                "Intelligence: " + this.primaryAttributes.intelligence + "\n" + "DPS: " + getCharacterDPS() + "\n" + ".......................";

        System.out.println(stats);
    }

    //Method to equip a weapon. It calls on the method checkEquipWeapon with the weapon as argument. That method checks if the character is allowed to equip it or not.
    // If not, an exception is thrown with we catch in this method. If everything is ok, the weapon is added to the characters slot.
    public void equipWeapon(Weapon weapon) {
        try {
            checkEquipWeapon(weapon);
            this.equipment.put(Slot.WEAPON, weapon);
        } catch (Exception e) {
            System.out.println("A problem occurred: " + e);
        }
    }

    //Method to add armor to the character. This basically works the same way as equipWeapon, other than that when the armor is placed in the enumMap,
    // I'm checking the armors slot to put it in the right place.
    public void equipArmor(Armor armor) {
        try {
            checkEquipArmor(armor);
            this.equipment.put(armor.getSlot(),armor);
            setPrimaryAttributes(armor.getPrimaryAttributes());
        } catch (Exception e) {
            System.out.println("A problem occurred: " + e);
        }
    }

    //Method to set the total primary attributes that armor contributes to. If the character has an armor piece equipped, it's attributes adds to the characters.
    public void setTotalPrimaryAttributes() {
        Armor headItem = (Armor) equipment.get(Slot.HEAD);
        Armor bodyItem = (Armor) equipment.get(Slot.BODY);
        Armor legsItem = (Armor) equipment.get(Slot.LEGS);

        int primaryAttributesFromArmor = 0;
        if (headItem != null) {
            primaryAttributesFromArmor += headItem.getPrimaryAttributes().getAttribute(getPrimaryAttributeType());
        }
        if (bodyItem != null) {
            primaryAttributesFromArmor += bodyItem.getPrimaryAttributes().getAttribute(getPrimaryAttributeType());
        }
        if (legsItem != null) {
            primaryAttributesFromArmor += legsItem.getPrimaryAttributes().getAttribute(getPrimaryAttributeType());
        }

        setTotalPrimaryAttributes(primaryAttributesFromArmor);

    }

    //Method to get a characters damage per second.
    public double getCharacterDPS() {
        double weaponDamagePerSecond = 1;
        Weapon weaponItem = (Weapon) equipment.get(Slot.WEAPON);

        if (weaponItem != null) {
            weaponDamagePerSecond = weaponItem.getDamagePerSecond();
        }
        int totalMainPrimaryAttributes = getTotalPrimaryAttributes();

        return (weaponDamagePerSecond * (1 + (totalMainPrimaryAttributes/100)));
    }
}
