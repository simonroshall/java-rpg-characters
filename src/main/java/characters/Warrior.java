package characters;

import exceptions.InvalidArmorException;
import exceptions.InvalidWeaponException;
import items.Armor;
import items.Weapon;

public class Warrior extends Character {
    public Warrior() {
    }

    public Warrior(String name) {
        super(name,1,5,0,"strength",5,2,1);
    }

    //Method to level up. As the stats differs depending on the character, I use an interface to override the method to change it depending on the character.
    @Override
    public void levelUp() {
        setLevel();
        this.basePrimaryAttributes += 3;
        this.totalPrimaryAttributes += 3;
        this.primaryAttributes.strength += 3;
        this.primaryAttributes.dexterity += 2;
        this.primaryAttributes.intelligence += 1;
    }
    //Method to check if a character is able to equip a weapon. In the if statement, I check that the characters level is equal or higher than the required level of the weapon,
    //if the weapon slot is WEAPON and if the weapon type is one that the specific character is able to equip.
    //If the character isn't allowed to equip the weapon, the custom exception InvalidWeaponException is thrown.
    @Override
    public void checkEquipWeapon(Weapon weapon) throws InvalidWeaponException {
        if(this.getLevel() >= weapon.getRequiredLevel() && weapon.getSlot().toString().equals("WEAPON") && weapon.getWeaponType().toString().equals("AXE") || weapon.getWeaponType().toString().equals("HAMMER") || weapon.getWeaponType().toString().equals("SWORD")){
            System.out.println(weapon.getName() + " is equipped!");
        }else{
            throw new InvalidWeaponException("\n" + "You cannot add this weapon!");
        }
    }
    //Method to check if a character is able to equip an armor. In the if statement, I check that the characters level is equal or higher than the required level of the armor,
    //and if the armor type is one that the specific character is able to equip.
    //If the character isn't allowed to equip the weapon, the custom exception InvalidArmorException is thrown.
    @Override
    public void checkEquipArmor(Armor armor) throws InvalidArmorException {
        if(armor.getArmorType().toString().equals("MAIL") || armor.getArmorType().toString().equals("PLATE") && this.getLevel() >= armor.getRequiredLevel()) {
            System.out.println(armor.getName() + " is equipped!");
        }else {
            throw new InvalidArmorException("\n" + "You cannot add this armor!");
        }
    }
}
