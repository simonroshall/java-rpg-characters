package characters;

import exceptions.InvalidArmorException;
import exceptions.InvalidWeaponException;
import items.Armor;
import items.Weapon;

public class Mage extends Character {

    public Mage() {
    }

    public Mage(String name) {
        super(name, 1, 8, 0,"intelligence",1,1,8);
    }

    @Override
    public void levelUp() {
        this.level++;
        this.basePrimaryAttributes += 5;
        this.totalPrimaryAttributes +=5;
        this.primaryAttributes.strength += 1;
        this.primaryAttributes.dexterity += 1;
        this.primaryAttributes.intelligence += 5;
    }

    @Override
    public void checkEquipWeapon(Weapon weapon) throws InvalidWeaponException {
        if(weapon.getWeaponType().toString().equals("STAFF") || weapon.getWeaponType().toString().equals("WAND") && weapon.getSlot().toString().equals("WEAPON") && this.getLevel() >= weapon.getRequiredLevel()) {
            System.out.println(weapon.getName() + " is equipped!");
        }else{
            throw new InvalidWeaponException("\n" + "You cannot add this weapon!");
        }
    }

    @Override
    public void checkEquipArmor(Armor armor) throws InvalidArmorException {
        if(armor.getArmorType().toString().equals("CLOTH") && this.getLevel() >= armor.getRequiredLevel()) {
            System.out.println(armor.getName() + " is equipped!");
        }else {
            throw new InvalidArmorException("\n" + "You cannot add this armor!");
        }
    }
}
