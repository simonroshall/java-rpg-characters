package characters;

import exceptions.InvalidArmorException;
import exceptions.InvalidWeaponException;
import items.Armor;
import items.Weapon;

public interface ICharacter {
    void levelUp();
    void checkEquipWeapon(Weapon weapon) throws InvalidWeaponException;
    void checkEquipArmor(Armor armor) throws InvalidArmorException;
}
